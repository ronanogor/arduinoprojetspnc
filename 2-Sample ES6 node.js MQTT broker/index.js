// This MQTT broker is configured to serve WebSocket and direct MQTT connexion
// see : ./lib/mqttserver for this dual-head configuration

const debug = require('debug')('mqtt:ctl')
require('./config')
const ora = require('ora')

class Ctl {
	constructor() {
		this.init()
	}

	async init() {
		let spinner
		try {
			spinner = ora(`Starting MQTT broker`).start();
			await require('./lib/mqttserver')
			spinner.succeed(`MQTT broker listening on MQTT://localhost:${process.env.MQTT_PORT} and HTTP://localhost:${process.env.WS_MQTT_PORT}`)

		} catch (error) {
			spinner.fail('Unable to start MQTT broker')
			debug(error)
			process.exit(1)
		}
	}
}

new Ctl()
