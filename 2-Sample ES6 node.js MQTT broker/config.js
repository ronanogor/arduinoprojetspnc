require('dotenv').config()
const debug = require('debug')('mqtt:config')

function noop() {}
function ifHasNotThrow(element, error) {
    if (!element) throw error
    return element
}
function ifHas(element, defaultValue) {
    if (!element) return defaultValue
    return element
}
function configureDefaults() {
    try {
		process.env.MQTT_PORT = ifHas(process.env.MQTT_PORT, 8999)
		process.env.WS_MQTT_PORT = ifHas(process.env.WS_MQTT_PORT, 8998)
    } catch (e) {
        console.error(e)
        process.exit(1)
    }
}
module.exports = configureDefaults()