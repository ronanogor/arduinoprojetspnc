const mosca = require("mosca")
const debug = require("debug")("mqtt:server")
class MqttServer {
	constructor() {
		// uses levelDb local store as a backend for messages and retained messages
		// Mosca can use redis, direct memory...
		this.moscaSettings = {
			port: parseInt(process.env.MQTT_PORT),
			persistence: {
				factory: 'levelup',
				path: './brokerDb',
				ttl: {
					checkFrequency: 6,
					subscriptions: 20,
					packets: 20
				}
			},
			http: {
				port: parseInt(process.env.WS_MQTT_PORT),
				bundle: true,
				static: './'
			}
		}
		return this.init()
	}

	async init() {
		return new Promise((resolve, reject) => {
			try {
				this.server = new mosca.Server(this.moscaSettings)
				this.server.on('published', (packet, client) => { })
				this.server.on('subscribe', (packet, client) => { })
				this.server.on('clientConnected', (client) => { debug(`New client connected ${client}`) })
				this.server.on('clientDisconnected', (client) => { })
				this.server.on('ready', () => resolve(this))
			} catch (e) {
				reject(e)
			}
		})
	}

}
module.exports = new MqttServer()
