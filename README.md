# arduinoProjetsPNC

## nodeMqttBroker

Uses node.js v9+.
Windows 10 user shall use an Ubuntu or Debian, see : https://docs.microsoft.com/en-us/windows/wsl/install-win10

```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
```

Set your env in .env file - see .envdefaults - or override like this
```bash
DEBUG=mqtt:* node index.js
```

## arduino MQTT client

__some doc__ :
* https://pubsubclient.knolleary.net/api.html
* https://github.com/knolleary/pubsubclient/tree/master/examples
* https://github.com/bblanchon/ArduinoJson/issues/357 (Seems like there's now a functional string / json implementation)
