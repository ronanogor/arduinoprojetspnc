// uses MQTT client directly form the browser through a WebSocket broker connexion
// see https://github.com/mqttjs/MQTT.js#browser
// ! \\ Vanilla old-fashioned JS style, as you might encounter in lots of legacy-code
// ! \\ modern Javascript shall be written using E6+ and eventualy transpiled with webpack and Babel
// see :
//			https://developer.mozilla.org/en-US/docs/Web/JavaScript/Language_Resources
//			https://webpack.js.org/concepts/
//			https://github.com/babel/babel-loader
//			for node JS : https://node.green/#ES2015

function App() {
	var self = this;

	this.connect = function (address) {
		this.client = mqtt.connect(address);
		console.log(this.client)
		this.client.subscribe("robot/#")
		this.client.on("message", function (topic, payload) {
			// do stuff on robot's sends
			// Payload is a buffer Object
			document.getElementById("receive").innerHTML = ("MQTT topic " + topic + "</br>MQTT payload" + payload.toString());
		});

		this.client.on("close", function (err) {
			document.getElementById("receive").innerHTML = ("Can't connect");
			self.client.end();
		});

		this.client.on("connect", function (err) {
			document.getElementById("receive").innerHTML = ("successfully connected to MQTT over WS.<br/> Listening to robots/# topics");
		});
	}

	this.left = function () {
		var direction = {
			"value": "left"
		};
		self.client.publish("robot/direction", JSON.stringify(direction));
	}
	this.right = function () {
		var direction = {
			"value": "right"
		};
		self.client.publish("robot/direction", JSON.stringify(direction));
	}
	this.accelerate = function () {
		var direction = {
			"value": "forward"
		};
		self.client.publish("robot/direction", JSON.stringify(direction));
	}
	this.reverse = function () {
		var direction = {
			"value": "reverse"
		};
		self.client.publish("robot/direction", JSON.stringify(direction));
	}
	this.stop = function () {
		var direction = {
			"value": "stop"
		};
		self.client.publish("robot/direction", JSON.stringify(direction));
	}

}

var app = new App();